<?xml version="1.0" encoding="UTF-8"?>
<interface>
  <template class="SecurityPage" parent="AdwPreferencesPage">
    <property name="icon-name">security-symbolic</property>
    <property name="title" translatable="yes">Security</property>
    <property name="name">security</property>
    <child>
      <object class="AdwPreferencesGroup">
        <property name="title" translatable="yes">Privacy</property>
        <child>
          <object class="AdwSwitchRow" id="public_read_receipts_row">
            <property name="selectable">False</property>
            <property name="title" translatable="yes">Send Read Receipts</property>
            <property name="subtitle" translatable="yes">Allow other members of the rooms you participate in to track which messages you have seen</property>
          </object>
        </child>
        <child>
          <object class="AdwSwitchRow" id="typing_row">
            <property name="selectable">False</property>
            <property name="title" translatable="yes">Send Typing Notifications</property>
            <property name="subtitle" translatable="yes">Allow other members of the rooms you participate in to see when you are typing a message</property>
          </object>
        </child>
        <child>
          <object class="ButtonCountRow" id="ignored_users_row">
            <property name="title" translatable="yes">Ignored Users</property>
            <property name="subtitle" translatable="yes">All messages or invitations sent by these users will be ignored. You will still see some of their activity, like when they join or leave a room.</property>
            <property name="action-name">account-settings.show-subpage</property>
            <property name="action-target">'ignored-users'</property>
          </object>
        </child>
      </object>
    </child>
    <child>
      <object class="AdwPreferencesGroup">
        <property name="title" translatable="yes">Crypto Identity</property>
        <property name="description" translatable="yes">Allows you to verify other Matrix accounts and automatically trust their verified sessions</property>
        <child>
          <object class="AdwPreferencesRow" id="crypto_identity_row">
            <property name="activatable">False</property>
            <property name="selectable">False</property>
            <accessibility>
              <relation name="labelled-by">crypto_identity_title</relation>
              <relation name="described-by">crypto_identity_description</relation>
            </accessibility>
            <property name="child">
              <object class="GtkBox">
                <property name="orientation">vertical</property>
                <property name="spacing">6</property>
                <property name="margin-start">12</property>
                <property name="margin-end">12</property>
                <property name="margin-top">12</property>
                <property name="margin-bottom">12</property>
                <child>
                  <object class="GtkImage" id="crypto_identity_icon">
                    <property name="accessible-role">presentation</property>
                    <property name="halign">center</property>
                    <style>
                      <class name="card-icon" />
                    </style>
                  </object>
                </child>
                <child>
                  <object class="GtkLabel" id="crypto_identity_title">
                    <property name="label" bind-source="crypto_identity_row" bind-property="title" bind-flags="sync-create" />
                    <property name="wrap">True</property>
                    <property name="wrap-mode">word-char</property>
                    <style>
                      <class name="heading" />
                    </style>
                  </object>
                </child>
                <child>
                  <object class="GtkLabel" id="crypto_identity_description">
                    <property name="wrap">True</property>
                    <property name="wrap-mode">word-char</property>
                    <style>
                      <class name="caption" />
                    </style>
                  </object>
                </child>
                <child>
                  <object class="GtkButton" id="crypto_identity_btn">
                    <property name="halign">center</property>
                    <property name="margin-top">6</property>
                    <property name="can-shrink">True</property>
                    <property name="action-name">account-settings.show-subpage</property>
                    <property name="action-target">'crypto-identity-setup'</property>
                  </object>
                </child>
              </object>
            </property>
          </object>
        </child>
      </object>
    </child>
    <child>
      <object class="AdwPreferencesGroup">
        <property name="title" translatable="yes">Account Recovery</property>
        <property name="description" translatable="yes">Allows to fully recover your account with a recovery key or passphrase, if you ever lose access to all your sessions</property>
        <child>
          <object class="AdwPreferencesRow" id="recovery_row">
            <property name="activatable">False</property>
            <property name="selectable">False</property>
            <accessibility>
              <relation name="labelled-by">recovery_title</relation>
              <relation name="described-by">recovery_description</relation>
            </accessibility>
            <property name="child">
              <object class="GtkBox">
                <property name="orientation">vertical</property>
                <property name="spacing">6</property>
                <property name="margin-start">12</property>
                <property name="margin-end">12</property>
                <property name="margin-top">12</property>
                <property name="margin-bottom">12</property>
                <child>
                  <object class="GtkImage" id="recovery_icon">
                    <property name="accessible-role">presentation</property>
                    <property name="halign">center</property>
                    <style>
                      <class name="card-icon" />
                    </style>
                  </object>
                </child>
                <child>
                  <object class="GtkLabel" id="recovery_title">
                    <property name="label" bind-source="recovery_row" bind-property="title" bind-flags="sync-create" />
                    <property name="wrap">True</property>
                    <property name="wrap-mode">word-char</property>
                    <style>
                      <class name="heading" />
                    </style>
                  </object>
                </child>
                <child>
                  <object class="GtkLabel" id="recovery_description">
                    <property name="wrap">True</property>
                    <property name="wrap-mode">word-char</property>
                    <style>
                      <class name="caption" />
                    </style>
                  </object>
                </child>
                <child>
                  <object class="GtkButton" id="recovery_btn">
                    <property name="halign">center</property>
                    <property name="margin-top">6</property>
                    <property name="can-shrink">True</property>
                    <property name="action-name">account-settings.show-subpage</property>
                    <property name="action-target">'recovery-setup'</property>
                  </object>
                </child>
              </object>
            </property>
          </object>
        </child>
        <child>
          <object class="GtkListBox">
            <property name="margin-top">12</property>
            <style>
              <class name="boxed-list" />
            </style>
            <child>
              <object class="AdwButtonRow">
                <property name="selectable">False</property>
                <!-- Translators: 'Room encryption keys' are encryption keys for all rooms. -->
                <property name="title" translatable="yes">Export Room Encryption Keys</property>
                <property name="end-icon-name">go-next-symbolic</property>
                <property name="action-name">account-settings.show-subpage</property>
                <property name="action-target">'export-keys'</property>
              </object>
            </child>
            <child>
              <object class="AdwButtonRow">
                <property name="selectable">False</property>
                <!-- Translators: 'Room encryption keys' are encryption keys for all rooms. -->
                <property name="title" translatable="yes">Import Room Encryption Keys</property>
                <property name="end-icon-name">go-next-symbolic</property>
                <property name="action-name">account-settings.show-subpage</property>
                <property name="action-target">'import-keys'</property>
              </object>
            </child>
          </object>
        </child>
      </object>
    </child>
  </template>
</interface>
