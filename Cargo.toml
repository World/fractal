[package]
name = "fractal"
version = "10.1.0"
authors = ["Julian Sparber <julian@sparber.net>"]
edition = "2021"
rust-version = "1.82"
publish = false

[package.metadata.cargo-machete]
ignored = ["serde_bytes"] # Used by the SecretFile API.

[profile.release]
debug = true
lto = "thin"
codegen-units = 1

[profile.dev.package."*"]
opt-level = 3
debug = false
debug-assertions = false
overflow-checks = false
incremental = false
codegen-units = 16

# Please keep dependencies sorted.
[dependencies]
async-once-cell = "0.5"
cfg-if = "1"
diff = "0.1"
djb_hash = "0.1"
futures-channel = "0.3"
futures-util = "0.3"
geo-uri = "0.2"
gettext-rs = { version = "0.7", features = ["gettext-system"] }
indexmap = "2"
linkify = "0.10.0"
mime = "0.3"
mime_guess = "2"
pulldown-cmark = "0.12"
qrcode = { version = "0.14", default-features = false }
rand = "0.9"
regex = "1"
rmp-serde = "1"
secular = { version = "1", features = ["bmp", "normalization"] }
serde = "1"
serde_bytes = "0.11"
serde_json = "1"
strum = { version = "0.27.1", features = ["derive"] }
tempfile = "3"
thiserror = "2"
tld = "2"
tokio = { version = "1", features = ["rt", "rt-multi-thread", "sync"] }
tokio-stream = { version = "0.1", features = ["sync"] }
tracing = "0.1"
tracing-subscriber = { version = "0.3", features = ["env-filter"] }
url = "2"
webp = { version = "0.3", default-features = false }
zeroize = "1"

# gtk-rs project and dependents. These usually need to be updated together.
adw = { package = "libadwaita", version = "0.7", features = ["v1_6"] }
glycin = { version = "2.1.0-rc", default-features = false, features = ["tokio", "gdk4"] }
gst = { version = "0.23", package = "gstreamer" }
gst_app = { version = "0.23", package = "gstreamer-app" }
gst_pbutils = { version = "0.23", package = "gstreamer-pbutils" }
gst_play = { version = "0.23", package = "gstreamer-play" }
gst_video = { version = "0.23", package = "gstreamer-video" }
gtk = { package = "gtk4", version = "0.9", features = ["gnome_47"] }
shumate = { package = "libshumate", version = "0.6" }
sourceview = { package = "sourceview5", version = "0.9" }

[dependencies.matrix-sdk]
# version = "0.10"
git = "https://github.com/matrix-org/matrix-rust-sdk.git"
rev = "2ac3b6e9a23c938f30f3b502cebb93da5282d9d3"
features = [
    "socks",
    "sso-login",
    "markdown",
    "qrcode",
]

[dependencies.matrix-sdk-store-encryption]
# version = "0.10"
git = "https://github.com/matrix-org/matrix-rust-sdk.git"
rev = "2ac3b6e9a23c938f30f3b502cebb93da5282d9d3"

[dependencies.matrix-sdk-ui]
# version = "0.10"
git = "https://github.com/matrix-org/matrix-rust-sdk.git"
rev = "2ac3b6e9a23c938f30f3b502cebb93da5282d9d3"

[dependencies.ruma]
# version = "0.12.1"
git = "https://github.com/ruma/ruma.git"
rev = "7755c7cbc580f8d8aea30d78cc1a6850b1a6fd39"
features = [
    "unstable-unspecified",
    "client-api-c",
    "compat-server-signing-key-version",
    "compat-user-id",
    "compat-empty-string-null",
    "compat-null",
    "compat-optional",
    "compat-unset-avatar",
    "html-matrix",
    "unstable-msc3824",
]

# Linux-only dependencies.
[target.'cfg(target_os = "linux")'.dependencies]
aperture = "0.9"
ashpd = { version = "0.11", default-features = false, features = [
    "tracing",
    "tokio",
] }
oo7 = { version = "0.3", default-features = false, features = [
    "openssl_crypto",
    "tokio",
    "tracing",
] }

[dev-dependencies]
assert_matches2 = "0.1"

[lints.clippy]
pedantic = { level = "warn", priority = -1 }
cast_possible_truncation = "allow"
cast_precision_loss = "allow"
default_trait_access = "allow"
module_name_repetitions = "allow"
new_without_default = "allow"
struct_field_names = "allow"
unsafe_derive_deserialize = "allow"
wildcard_imports = "allow"
